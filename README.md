#IVR Definition
This repository contains the IVR definition file for the medication reconciliation app.  It 
is generated by selecting Voxeo CXP -> Export Project Version from within Voxeo CXP, and it 
can be similarly imported by selecting the XML file after clicking Voxeo CXP -> Import.

Be sure to look at the branches for different features.  The `vitamin_feature` branch has a questionnaire about a patient's OTC vitamin usage at the end of the call.

# Backend
The IVR depends on a backend service to retrieve a patient's medication info.  The service is split into two repos and currently resides [here](https://bitbucket.org/mdnukem/ivr_api) and [here](https://bitbucket.org/mdnukem/ivrsvc).

You will almost certainly need to configure the IVR VoiceXML from within Voxeo CXP to point to the correct location for the IVR service, but otherwise, just right click the `Components`->`Module`->`_START` module and select `Test Application` from within Voxeo CXP to start the IVR on your local machine.